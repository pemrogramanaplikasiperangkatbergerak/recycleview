package com.virna.recycleview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    EditText etNIM, etName;
    Button btnTambah;
    Toolbar toolbar;
    public static String TAG = "RV1";
    MahasiswaAdapter adapter;
    ArrayList<Mahasiswa>data= new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("225150407111010");

        etNIM = findViewById(R.id.etNim);
        etName = findViewById(R.id.etNama);
        btnTambah = findViewById(R.id.button);
        rv1 = findViewById(R.id.rv1);

        adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        btnTambah.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String nim = etNIM.getText().toString();
                String nama = etName.getText().toString();
                tambahData(nim, nama);
                etNIM.setText("");
                etName.setText("");
            }
        });
    }
    public void tambahData(String nim, String nama){
        Mahasiswa mhs = new Mahasiswa();
        mhs.nim = nim;
        mhs.nama = nama;
        adapter.addData(mhs);
        adapter.notifyDataSetChanged();
    }
}
